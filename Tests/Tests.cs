using NUnit.Framework;

namespace dotnetcore.Tests;

public class Tests
{
    [Test]
    public void PassingTest()
    {
        Assert.AreEqual(4, Calculator.Add(2,2));
    }

    [Test]
    public void FailingTest()
    {
        Assert.AreEqual(4, Calculator.Add(2,2));
    }
}